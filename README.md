# GraphQL Export

GraphQL Export enables exporting your GraphQL 4.x schemas to a file.

This module enables:

- Admin Web UI for exporting.
- Drush config:export hook for exporting GraphQL 4.x schemas on config export.

## Web UI

This module will also expose a simple interface at `/admin/config/graphql/servers/manage/SERVER_MACHINE_ID_HERE/export` where you can see and manually export the schema.

## Exporting with Drush

Add the following to your `settings.php`:

```php
$settings['graphql_export'] = [
  'SERVER_MACHINE_ID_HERE' => [
    'graphqls' => '../schema.graphqls',
    'json' => '../schema.json',
  ],
];
```

You can change the file (and file type) path to whatever you like, as long as the path is writable by Drupal when exporting your config.

### Automatically exporting

When you're ready, just run `drush config:export` (or drush cex if you hate typing) and the full introspection result will be put into the specified file(s). This functionality can be disabled by adding the setting `skip_config_export` as `true`.

```php
$settings['graphql_export'] = [
  'SERVER_MACHINE_ID_HERE' => [
    // ...
    'skip_config_export' => true,
  ],
];
```

### Manually exporting

You can also export the schema manually by running `drush graphql-export:schema` command.

See `drush help graphql-export:schema` for more information.
