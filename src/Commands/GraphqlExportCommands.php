<?php

declare(strict_types=1);

namespace Drupal\graphql_export\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Site\Settings;
use Drupal\graphql\Entity\Server;
use Drupal\graphql\Entity\ServerInterface;
use Drupal\graphql_export\GraphqlExportService;
use Drush\Commands\DrushCommands;

/**
 * Adds the post-command hook to the config:export command.
 */
class GraphqlExportCommands extends DrushCommands {

  /**
   * Construct a new export controller.
   *
   * @param \Drupal\graphql_export\GraphqlExportService $exportService
   *   The export service.
   */
  public function __construct(
    protected GraphqlExportService $exportService,
  ) {}

  /**
   * Get the settings defined in the configuration (settings.php).
   */
  protected function getSettings(): array {
    return Settings::get('graphql_export', []);
  }

  /**
   * Export the GraphQL Schemas on config:export.
   *
   * @hook post-command config:export
   */
  public function postCommand($result, CommandData $commandData): void {
    $settings = $this->getSettings();
    $server_ids = array_keys($settings);
    $servers = $server_ids ? Server::loadMultiple($server_ids) : [];

    foreach ($servers as $server) {
      $setting = $settings[$server->id()] ?? [];

      if (empty($setting) || !empty($setting['skip_config_export'])) {
        continue;
      }

      $this->exportServer(
        $server,
        $setting['graphqls'] ?? NULL,
        $setting['json'] ?? NULL
      );
    }
  }

  /**
   * Export the GraphQL Schemas manually.
   *
   * @param string|null $server_ids
   *   The server id(s) to export.
   * @param array $options
   *   The options via --option input.
   *
   * @command graphql-export:schema
   * @option type The type of schema to export. Defaults to both graphqls and json. (e.g json, graphqls)
   *
   * @usage graphql-export:schema --type=graphqls
   *   Export the schema as graphqls.
   * @usage graphql-export:schema --type=json my_server
   *   Export the schema for a specific server as JSON.
   * @usage graphql-export:schema my_server,and_another
   *   Export the schema for a specific server(s).
   */
  public function exportCommand(?string $server_ids = NULL, $options = ['type' => NULL]): void {
    $settings = $this->getSettings();
    $type = $options['type'];

    // Determine the servers to export.
    // If no server is provided, export all servers.
    $server_ids = $server_ids ? explode(',', $server_ids) : NULL;
    $servers = Server::loadMultiple($server_ids);

    // Determine the export type.
    $export_json = $type === 'json' || !$type;
    $export_graphqls = $type === 'graphqls' || !$type;

    foreach ($servers as $server) {
      $setting = $settings[$server->id()] ?? [];

      // Export the schema to the default location if no custom path is set.
      $graphqls_filename = $setting['graphqls'] ?? 'private://' . $server->id() . '.graphqls';
      $json_filename = $setting['json'] ?? 'private://' . $server->id() . '.json';

      $this->exportServer(
        $server,
        $export_graphqls ? $graphqls_filename : NULL,
        $export_json ? $json_filename : NULL
      );
    }

    $this->logger()->success(dt('GraphQL Export: Complete'));
  }

  /**
   * Export the server.
   *
   * @param \Drupal\graphql\Entity\ServerInterface $server
   *   The GraphQL server.
   * @param string|null $graphqls_filename
   *   The filename for the GraphQL schema.
   * @param string|null $json_filename
   *   The filename for the JSON schema.
   */
  protected function exportServer(ServerInterface $server, ?string $graphqls_filename, ?string $json_filename): void {
    try {
      if ($graphqls_filename) {
        $data = $this->exportService->printSchema($server);
        $output = $this->writeFile($graphqls_filename, $data, $server->id() . '.graphqls');

        $this->logger()->success(dt('GraphQL Export: Exported schema @id (@filename)', [
          '@id' => $server->id(),
          '@filename' => $output,
        ]));
      }
      if ($json_filename) {
        $data = $this->exportService->printJson($server);
        $output = $this->writeFile($json_filename, $data, $server->id() . '.json');

        $this->logger()->success(dt('GraphQL Export: Exported JSON @id (@filename)', [
          '@id' => $server->id(),
          '@filename' => $output,
        ]));
      }
    }
    catch (\Throwable $e) {
      $this->logger()->error(dt('GraphQL Export: Failed with error @message', [
        '@message' => $e->getMessage(),
      ]));
    }
  }

  /**
   * Write the file.
   *
   * @param string $path
   *   The path to write the file.
   * @param string $data
   *   The data to write.
   * @param string $default_filename
   *   The default filename if path is a directory.
   *
   * @return string
   *   The path to the file.
   *
   * @throws \RuntimeException
   */
  private function writeFile(string $path, string $data, string $default_filename): string {
    if (is_dir($path)) {
      $path = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $default_filename;
    }

    if (!file_put_contents($path, $data)) {
      throw new \RuntimeException(sprintf('Could not write GraphQL schema to %s', $path));
    }

    return $path;
  }

}
