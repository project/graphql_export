<?php

declare(strict_types=1);

namespace Drupal\graphql_export;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\graphql\Entity\Server;
use Drupal\graphql\Entity\ServerInterface;
use Drupal\graphql\GraphQL\Utility\Introspection;
use Drupal\graphql\Plugin\SchemaPluginInterface;
use Drupal\graphql\Plugin\SchemaPluginManager;
use GraphQL\Utils\SchemaPrinter;

/**
 * GraphQL Export Service.
 */
class GraphqlExportService {

  /**
   * Construct a new export controller.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\graphql\GraphQL\Utility\Introspection $introspection
   *   The introspection service.
   * @param \Drupal\graphql\Plugin\SchemaPluginManager $pluginManager
   *   The schema plugin manager.
   */
  public function __construct(
    protected MessengerInterface $messenger,
    protected Introspection $introspection,
    protected SchemaPluginManager $pluginManager,
  ) {}

  /**
   * Get the schema plugin for a server.
   *
   * @param \Drupal\graphql\Entity\ServerInterface $graphql_server
   *   The server.
   *
   * @return \Drupal\graphql\Plugin\SchemaPluginInterface
   *   The schema plugin.
   */
  private function getSchemaPlugin(ServerInterface $graphql_server): SchemaPluginInterface {
    $schema_name = $graphql_server->get('schema');

    /** @var \Drupal\graphql\Plugin\SchemaPluginInterface $plugin */
    $plugin = $this->pluginManager->createInstance($schema_name);
    $config = $graphql_server->get('schema_configuration');
    if ($plugin instanceof ConfigurableInterface && $config) {
      $plugin->setConfiguration($config[$schema_name] ?? []);
    }

    return $plugin;
  }

  /**
   * Get the schema for a server.
   *
   * @param \Drupal\graphql\Entity\ServerInterface $graphql_server
   *   The server.
   *
   * @return string
   *   The schema.
   */
  public function printSchema(ServerInterface $graphql_server): string {
    $plugin = $this->getSchemaPlugin($graphql_server);

    $schema = $plugin->getSchema($plugin->getResolverRegistry());
    $printed = SchemaPrinter::doPrint($schema);

    return $printed;
  }

  /**
   * Print the schema as introspection JSON.
   *
   * @param \Drupal\graphql\Entity\ServerInterface $graphql_server
   *   The server.
   *
   * @return string|null
   *   The introspection JSON.
   */
  public function printJson(ServerInterface $graphql_server): ?string {
    if ($graphql_server instanceof Server) {
      $graphql_server->setQueryDepth(NULL);
      $graphql_server->setQueryComplexity(NULL);
      $graphql_server->setDisableIntrospection(FALSE);
    }
    $introspection = $this->introspection->introspect($graphql_server);
    $printed = json_encode($introspection, JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);

    return $printed ?: NULL;
  }

}
