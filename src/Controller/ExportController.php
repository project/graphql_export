<?php

declare(strict_types=1);

namespace Drupal\graphql_export\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\graphql\Entity\ServerInterface;
use Drupal\graphql_export\GraphqlExportService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller GraphQL Schema Exporter.
 */
class ExportController implements ContainerInjectionInterface {
  use StringTranslationTrait;

  /**
   * Construct a new export controller.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\graphql_export\GraphqlExportService $exportService
   *   The export service.
   */
  public function __construct(
    protected MessengerInterface $messenger,
    protected GraphqlExportService $exportService,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('messenger'),
      $container->get('graphql_export.service'),
    );
  }

  /**
   * View an exported schema.
   *
   * @param \Drupal\graphql\Entity\ServerInterface $graphql_server
   *   The server.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   *   The render array.
   */
  public function viewExporter(ServerInterface $graphql_server, Request $request): array {

    try {
      $content = $this->exportService->printSchema($graphql_server);
    }
    catch (\Exception $e) {
      $this->messenger->addError($e->getMessage());
    }

    $settings = Settings::get('graphql_export', []);

    if (array_key_exists($graphql_server->id(), $settings)) {
      $this->messenger->addMessage($this->t('This service will export with drush config:export.'), 'info');
    }

    return [
      'schema' => [
        '#type' => 'textarea',
        '#value' => $content,
        '#rows' => 20,
        '#attributes' => [
          'readonly' => 'readonly',
          'class' => ['graphql-export'],
        ],
      ],
      'graphqls' => [
        '#type' => 'link',
        '#title' => $this->t('Download GraphQL schema'),
        '#url' => Url::fromRoute('graphql_export.export_download_graphqls', ['graphql_server' => $graphql_server->id()]),
        '#attributes' => [
          'class' => ['button', 'button--small'],
        ],
      ],
      'json' => [
        '#type' => 'link',
        '#title' => $this->t('Download introspection json'),
        '#url' => Url::fromRoute('graphql_export.export_download_json', ['graphql_server' => $graphql_server->id()]),
        '#attributes' => [
          'class' => ['button', 'button--small'],
        ],
      ],
      '#attached' => [
        'library' => [
          'graphql_export/export',
        ],
      ],
    ];
  }

  /**
   * Download an exported schema.
   *
   * @param \Drupal\graphql\Entity\ServerInterface $graphql_server
   *   The server.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The download response.
   */
  public function downloadGraphqls(ServerInterface $graphql_server, Request $request): Response {

    $content = $this->exportService->printSchema($graphql_server);

    $headers = [
      'Content-Type' => 'application/octet-stream',
      'Content-Disposition' => 'attachment; filename="schema.graphqls"',
    ];

    return new Response($content, 200, $headers, TRUE);
  }

  /**
   * Download an exported schema.
   *
   * @param \Drupal\graphql\Entity\ServerInterface $graphql_server
   *   The server.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The download response.
   */
  public function downloadJson(ServerInterface $graphql_server, Request $request): Response {

    $content = $this->exportService->printJson($graphql_server);

    $headers = [
      'Content-Type' => 'application/octet-stream',
      'Content-Disposition' => 'attachment; filename="schema.json"',
    ];

    return new Response($content, 200, $headers, TRUE);
  }

}
